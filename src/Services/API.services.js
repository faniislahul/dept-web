import axios from 'axios';

const BASE_URL = 'https://simple-server-001221.herokuapp.com';

export const get = (params, onSuccess, onFailed) => {
  axios.get(`${BASE_URL}/${params}`).then(onSuccess).catch(onFailed);
};

export default {
  get
};
