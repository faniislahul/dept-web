import ABN from './ABN_logo_zwart.jpg';
import Adidas from './Adidas_logo_zwart.jpg';
import KLM from './KLM_logo_zwart.jpg';
import Microsoft from './Microsoft_logo_zwart-320x161.jpg';
import Mona from './Mona_logo_zwart.jpg';
import Nivea from './Nivea_logo_zwart.jpg';
import NN from './NN_logo_zwart.jpg';
import Oxxio from './Oxxio_logo_zwart.jpg';
import Pathe from './Pathe_logo_zwart.jpg';
import Tomtom from './Tomtom_logo_zwart.jpg';
import Transavia from './Transavia_logo_zwart.jpg';
import Triumph from './Triumph_logo_zwart.jpg';
import Unilever from './Unilever_logo_zwart.jpg';
import Walibi from './Walibi_logo_zwart.jpg';
import Zalando from './Zalando_logo_zwart.jpg';
import Ziggo from './Ziggo_logo_zwart.jpg';

export default {
  ABN,
  Adidas,
  KLM,
  Microsoft,
  Mona,
  Nivea,
  NN,
  Oxxio,
  Pathe,
  Tomtom,
  Transavia,
  Triumph,
  Unilever,
  Walibi,
  Zalando,
  Ziggo
};
