import React from 'react';
import debounce from 'lodash/debounce';

import Menu from '../Menu';

import './Header.component.styles.scss';
import deptLogo from '../../Assets/Icons/icon-dept.svg';

const _onMenuClick = (show, setShow) => () => {
  setShow(!show);
};

const _renderMenuButton = (show, setShow) => (
  <div
    data-testid="menuButton"
    className="menuIconContainer"
    onClick={_onMenuClick(show, setShow)}
    onKeyPress={_onMenuClick(show, setShow)}
    tabIndex="0"
    role="button"
    focusable
  >
    <span className="menuText" data-show={show}>MENU</span>
    <div className="menuIcon" data-show={show}>
      <div className="menuIconBarTop" />
      <div className="menuIconBarBottom" />
    </div>
  </div>
);

const _onScroll = (setScroll) => debounce(() => {
  const scroll = (window.scrollY / document.documentElement.scrollHeight) * 100;

  if (scroll > 10) {
    setScroll(true);
  } else {
    setScroll(false);
  }
}, 200, true);

const useScrollEffect = (setScroll) => {
  React.useEffect(() => {
    window.addEventListener('scroll', _onScroll(setScroll));

    return () => window.removeEventListener('scroll', _onScroll(setScroll));
  }, []);
};

const Header = () => {
  const [show, setShow] = React.useState(false);
  const [scroll, setScroll] = React.useState(false);

  useScrollEffect(setScroll);

  return (
    <>
      <div className="headerContainer" data-show={show} data-scroll={scroll}>
        <div className="headerLine" data-show={show}>
          <img className="logo" src={deptLogo} alt="logo" data-show={show} />
          {_renderMenuButton(show, setShow)}
        </div>
      </div>
      <Menu show={show} />
    </>
  );
};

export default Header;
