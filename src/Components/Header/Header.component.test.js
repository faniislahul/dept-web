import React from 'react';
import { render, fireEvent } from '@testing-library/react';

import Header from './Header.component';

describe('Header Component test', () => {
  it('should return correct snapshot', () => {
    const { baseElement } = render(<Header />);

    expect(baseElement).toMatchSnapshot();
  });

  it('should call onClick function when clicked', () => {
    const mockState = false;
    const mockSetState = jest.fn();
    jest.spyOn(React, 'useState').mockReturnValue([mockState, mockSetState]);

    const { getByTestId } = render(<Header />);
    const component = getByTestId('menuButton');

    fireEvent.click(component);

    expect(mockSetState).toBeCalledWith(!mockState);
  });
});
