import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import find from 'lodash/find';
import get from 'lodash/get';

import { actions } from '../../Redux/Reducer/Projects.reducer';
import './Filter.component.styles.scss';

const categories = [
  { name: 'all works', value: null },
  { name: 'websites', value: 'websites' },
  { name: 'campaign', value: 'campaign' },
  { name: 'apps', value: 'apps' }
];

const industries = [
  { name: 'all industries', value: null },
  { name: 'banking', value: 'banking' },
  { name: 'hospitality', value: 'hospitality' },
  { name: 'education', value: 'education' },
  { name: 'manufacture', value: 'manufacture' },
  { name: 'consumer', value: 'consumer' }
];

const { setCategory, setIndustry } = actions;

const _onShowOptions = (show, setShow) => () => {
  setShow(!show);
};

const _onSelect = (dispatch, setter, value) => () => {
  dispatch(setter(value));
};

const _getCommonButtonProps = (className, action) => ({
  className,
  onClick: action,
  onKeyPress: action,
  role: 'button',
  tabIndex: '0',
  focusable: true,
});

const Filter = () => {
  const [showCategory, setShowCategory] = React.useState(false);
  const [showIndusty, setShowIndustry] = React.useState(false);
  const { industry, category } = useSelector((state) => state.project);
  const dispatch = useDispatch();

  return (
    <div className="filterContainer">
      <div className="filterItem">
        <span className="filterLabel">Show me </span>
        <div data-testid="filter-category" {..._getCommonButtonProps('filterSelect', _onShowOptions(showCategory, setShowCategory))}>
          <span>{get(find(categories, { value: category }), 'name', categories[0].name)}</span>
          <div className="arrow" data-show={showCategory} />
          <div className="selectOptions" data-show={showCategory}>
            {categories.map((item) => (
              <div
                data-testid={`options-${item.value}`}
                {..._getCommonButtonProps('', _onSelect(dispatch, setCategory, item.value))}
                key={item.value}
              >
                {item.name}
              </div>
            ))}
          </div>
        </div>
      </div>
      <div className="filterItem">
        <span className="filterLabel">in </span>
        <div {..._getCommonButtonProps('filterSelect', _onShowOptions(showIndusty, setShowIndustry))}>
          <span>{get(find(industries, { value: industry }), 'name', industries[0].name)}</span>
          <div className="arrow" data-show={showIndusty} />
          <div className="selectOptions" data-show={showIndusty}>
            {industries.map((item) => (
              <div {..._getCommonButtonProps('', _onSelect(dispatch, setIndustry, item.value))} key={item.value}>
                {item.name}
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Filter;
