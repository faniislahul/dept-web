import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { useSelector, useDispatch } from 'react-redux';

import Filter from './Filter.component';

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: jest.fn()
}));

jest.useFakeTimers();

describe('Filter Component test', () => {
  it('should return correct snapshot with category and industry empty', () => {
    useSelector.mockReturnValue({});

    const { container } = render(<Filter />);

    expect(container.firstChild).toMatchSnapshot();
  });

  it('should show category options when dropdown clicked', () => {
    const mockState = false;
    const mockSetState = jest.fn();
    jest.spyOn(React, 'useState').mockReturnValueOnce([mockState, mockSetState]);
    const { getByTestId } = render(<Filter />);
    const div = getByTestId('filter-category');

    fireEvent.click(div);

    expect(mockSetState).toBeCalledWith(true);
  });

  it('should dispatch action when dropdown options clicked', () => {
    const mockDispatch = jest.fn();
    useDispatch.mockReturnValue(mockDispatch);
    const { getByTestId } = render(<Filter />);
    const div = getByTestId('options-websites');

    fireEvent.click(div);

    expect(mockDispatch).toBeCalled();
  });
});
