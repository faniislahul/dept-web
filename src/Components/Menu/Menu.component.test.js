import React from 'react';
import { render, fireEvent } from '@testing-library/react';

import Menu from './Menu.component';

describe('Menu Component test', () => {
  const props = {
    show: true
  };

  it('should return correct snapshot', () => {
    const { container } = render(<Menu {...props} />);

    expect(container.firstChild).toMatchSnapshot();
  });

  it('should call onClick function when clicked', () => {
    const mockMenu = 'HOME';
    const mockState = '';
    const mockSetState = jest.fn();
    jest.spyOn(React, 'useState').mockReturnValue([mockState, mockSetState]);

    const { getByTestId } = render(<Menu {...props} />);
    const component = getByTestId(`menu-${mockMenu}`);

    fireEvent.click(component);

    expect(mockSetState).toBeCalledWith(mockMenu);
  });
});
