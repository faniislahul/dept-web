import React from 'react';
import PropTypes from 'prop-types';

import './Menu.component.styles.scss';

const menuList = [
  {
    title: 'HOME',
    uri: '/'
  },
  {
    title: 'WERK',
    uri: '#work'
  },
  {
    title: 'OVER',
    uri: '#over'
  },
  {
    title: 'DIESTEN',
    uri: '#diesten'
  },
  {
    title: 'PARTNERS',
    uri: '#careers'
  },
  {
    title: 'STORIES',
    uri: '#contact'
  },
  {
    title: 'VACATURES',
    uri: '#contact'
  }
];

const _onMenuClick = (setSelected, title) => () => {
  setSelected(title);
};

const _renderMenuList = (opacity, selected, setSelected) => (
  <div className="menuList">
    {menuList.map((item, index) => (
      <a
        className="menuItem"
        data-testid={`menu-${item.title}`}
        style={{ transitionDelay: `${index * 100}ms` }}
        href={item.uri}
        data-display={opacity}
        data-selected={item.title === selected}
        onClick={_onMenuClick(setSelected, item.title)}
        key={item.title}
      >
        {item.title}
      </a>
    ))}
  </div>
);

const renderRegionList = () => (
  <div className="regionSelect">
    <a href="#landen">LANDEN</a>
    <br />
    <a href="#landen">GLOBAL</a>
    <a href="#landen" data-selected>NETHERLAND</a>
    <a href="#landen">UNITED STATES</a>
    <a href="#landen">IRELAND</a>
    <a href="#landen">UNITED KINGDOM</a>
    <a href="#landen">DEUTSCHLAND</a>
    <a href="#landen">SCHWEIZ</a>
  </div>
);

const renderSocialList = () => (
  <div className="socialList">
    <a href="#fb">FACEBOOK</a>
    <a href="#twitter">TWITTER</a>
    <a href="#instagram">INSTAGRAM</a>
    <a href="#linkedin">LINKEDIN</a>
  </div>
);

const Menu = ({ show }) => {
  const [display, setDisplay] = React.useState(false);
  const [opacity, setOpacity] = React.useState(false);
  const [selected, setSelected] = React.useState(menuList[0].title);

  React.useEffect(() => {
    if (show) {
      setDisplay(true);
      setTimeout(() => setOpacity(true), 50);
    } else {
      setOpacity(false);
      setTimeout(() => setDisplay(false), 500);
    }
  }, [show]);

  return (
    <div className="menuContainer" data-opacity={opacity} data-display={display}>
      <div className="topShadow" />
      {_renderMenuList(opacity, selected, setSelected)}
      {renderRegionList()}
      {renderSocialList()}
      <div className="bottomShadow" />
    </div>
  );
};

Menu.propTypes = {
  show: PropTypes.bool.isRequired
};

export default Menu;
