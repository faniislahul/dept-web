import React from 'react';
import { render, fireEvent } from '@testing-library/react';

import Footer from './Footer.component';

jest.mock('../../Assets/Icons/icon-scroll-to-top.svg', () => ({
  ReactComponent: 'svg'
}));

describe('Footer Component test', () => {
  it('should return correct snapshot', () => {
    const { container } = render(<Footer />);

    expect(container.firstChild).toMatchSnapshot();
  });

  it('should call onClick function when clicked', () => {
    const mockFunction = jest.spyOn(window, 'scrollTo').mockImplementation(() => {});
    const mockParams = {
      behavior: 'smooth',
      top: 0
    };
    const { getByTestId } = render(<Footer />);
    const component = getByTestId('scrollToTopButton');

    fireEvent.click(component);

    expect(mockFunction).toBeCalledWith(mockParams);
  });
});
