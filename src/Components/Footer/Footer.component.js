import React from 'react';

import './Footer.component.styles.scss';
import deptLogo from '../../Assets/Icons/icon-dept.svg';
import iconFb from '../../Assets/Icons/icon-fb.svg';
import iconTwitter from '../../Assets/Icons/icon-twitter.svg';
import iconInstagram from '../../Assets/Icons/icon-instagram.svg';
import { ReactComponent as IconScroll } from '../../Assets/Icons/icon-scroll-to-top.svg';

const footerActionList = [
  {
    title: 'WORK',
    uri: '#work'
  },
  {
    title: 'SERVICES',
    uri: '#services'
  },
  {
    title: 'STORIES',
    uri: '#stories'
  },
  {
    title: 'ABOUT',
    uri: '#about'
  },
  {
    title: 'CAREERS',
    uri: '#careers'
  },
  {
    title: 'CONTACT',
    uri: '#contact'
  }
];

const ActionList = () => (
  <div className="actionList">
    {footerActionList.map((item) => (
      <a className="actionListItem" href={item.uri} key={item.title}>{item.title}</a>
    ))}
  </div>
);

const SocialMediaList = () => (
  <div className="actionList">
    <img className="socialIcon" src={iconFb} alt="fb-icon" />
    <img className="socialIcon" src={iconTwitter} alt="twitter-icon" />
    <img className="socialIcon" src={iconInstagram} alt="instagram-icon" />
  </div>
);

const _onScrollToTop = () => {
  window.scrollTo({
    top: 0,
    behavior: 'smooth'
  });
};

const _renderScrollToTop = () => (
  <div
    data-testid="scrollToTopButton"
    className="scrollToTopContainer"
    role="button"
    focusable
    tabIndex="0"
    onClick={_onScrollToTop}
    onKeyPress={_onScrollToTop}
  >
    <IconScroll className="scrollIcon" fill="#1a18f7" />
    <span>TOP</span>
  </div>
);

const _renderFooterMenu = () => (
  <div className="footerBorder">
    <img className="logoWhite" fill="#fff" src={deptLogo} alt="logo-white" />
    <ActionList />
    <SocialMediaList />
  </div>
);

const _renderFooterLinks = () => (
  <div className="footerLinks">
    <a href="#coc">Chamber of Commerce: 63464101</a>
    <a href="#vat">VAT: NL 8552.47.502.B01</a>
    <a href="#term-and-condition">Terms and conditions</a>
  </div>
);

const Footer = () => (
  <div className="footer">
    <div className="footerContent">
      {_renderFooterMenu()}
      {_renderFooterLinks()}
    </div>
    {_renderScrollToTop()}
  </div>
);

export default Footer;
