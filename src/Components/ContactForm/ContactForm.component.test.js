import React from 'react';
import { render, fireEvent } from '@testing-library/react';

import ContactForm from './ContactForm.component';

jest.useFakeTimers();

describe('ContactForm Component test', () => {
  const mockState = {
    name: '',
    email: '',
    message: '',
    errors: {}
  };
  const mockSetState = jest.fn();

  describe('Snapshots test', () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });

    it('should return correct snapshot', () => {
      const { container } = render(<ContactForm />);

      expect(container.firstChild).toMatchSnapshot();
    });

    it('should return correct snapshot with name input error', () => {
      const stateWithError = {
        ...mockState,
        errors: {
          name: true
        }
      };
      jest.spyOn(React, 'useState').mockReturnValue([stateWithError, mockSetState]);

      const { container } = render(<ContactForm />);

      expect(container.firstChild).toMatchSnapshot();
    });

    it('should return correct snapshot with email input error', () => {
      const stateWithError = {
        ...mockState,
        errors: {
          email: true
        }
      };
      jest.spyOn(React, 'useState').mockReturnValue([stateWithError, mockSetState]);

      const { container } = render(<ContactForm />);

      expect(container.firstChild).toMatchSnapshot();
    });

    it('should return correct snapshot with message input error', () => {
      const stateWithError = {
        ...mockState,
        errors: {
          message: true
        }
      };
      jest.spyOn(React, 'useState').mockReturnValue([stateWithError, mockSetState]);

      const { container } = render(<ContactForm />);

      expect(container.firstChild).toMatchSnapshot();
    });
  });

  describe('Form input test', () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });

    it('should setState with new value when form changed', () => {
      const key = 'name';
      const mockNewValue = 'John';
      const expectedResult = {
        ...mockState,
        [key]: mockNewValue,
        errors: {
          [key]: false
        }
      };
      jest.spyOn(React, 'useState').mockReturnValue([mockState, mockSetState]);
      const { getByTestId } = render(<ContactForm />);
      const input = getByTestId(`${key}-form`);

      fireEvent.change(input, { target: { value: mockNewValue } });

      expect(mockSetState).toBeCalledWith(expectedResult);
    });

    it('should validate form when form blur', () => {
      const key = 'name';
      const mockNewValue = 'John';
      const expectedResult = {
        errors: {
          [key]: false
        }
      };
      jest.spyOn(React, 'useState').mockReturnValue([mockState, mockSetState]);
      const { getByTestId } = render(<ContactForm />);
      const input = getByTestId(`${key}-form`);

      fireEvent.blur(input, { target: { value: mockNewValue } });

      expect(mockSetState).toBeCalledWith(expectedResult);
    });

    it('should validate form and set error when form blur and value is empty', () => {
      const key = 'name';
      const mockNewValue = '';
      const expectedResult = {
        errors: {
          [key]: true
        }
      };
      jest.spyOn(React, 'useState').mockReturnValue([mockState, mockSetState]);
      const { getByTestId } = render(<ContactForm />);
      const input = getByTestId(`${key}-form`);

      fireEvent.blur(input, { target: { value: mockNewValue } });

      expect(mockSetState).toBeCalledWith(expectedResult);
    });
  });
});
