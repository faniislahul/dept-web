import React from 'react';
import get from 'lodash/get';

import Button from '../Button';

import './ContactForm.component.styles.scss';

const regex = /^\w+([\\.-]?\w+)*@\w+([\\.-]?\w+)*(\.\w{2,3})+$/;
const _onChange = (form, setForm, key) => (evt) => {
  setForm({
    ...form,
    errors: { ...form.errors, [key]: false },
    [key]: evt.target.value
  });
};

const _onBlur = (form, setForm, key) => (evt) => {
  const value = get(evt, 'target.value');

  if (key === 'email' && (!value || value === ' ' || !regex.test(value))) {
    setForm({ errors: { ...form.errors, [key]: true } });

    return;
  }

  if (!value || value === ' ') {
    setForm({ errors: { ...form.errors, [key]: true } });

    return;
  }

  setForm({ errors: { ...form.errors, [key]: false } });
};

const _getInitialFormState = () => ({
  name: '',
  email: '',
  message: '',
  errors: {}
});

const _getFormCommonProps = (form, setForm, key) => ({
  name: key,
  value: form[key],
  onChange: _onChange(form, setForm, key),
  onBlur: _onBlur(form, setForm, key),
  'data-error': get(form, `errors.${key}`, false),
  'data-testid': `${key}-form`
});

const _renderError = (errors, key) => (
  get(errors, key) && <span data-error>{`Please input valid ${key}`}</span>
);

const ContactForm = () => {
  const [form, setForm] = React.useState(_getInitialFormState());
  const { errors } = form;

  return (
    <div className="contactContainer">
      <div>
        <h1 className="contactHeader">QUESTION? WE ARE HERE TO HELP!</h1>
      </div>
      <form className="form">
        <div className="contactForm">
          <div className="formInput">
            <span>NAME</span>
            <input type="text" {..._getFormCommonProps(form, setForm, 'name')} />
            {_renderError(errors, 'name')}
          </div>
          <div className="formInput">
            <span>EMAIL</span>
            <input type="email" {..._getFormCommonProps(form, setForm, 'email')} />
            {_renderError(errors, 'email')}
          </div>
          <div className="formInput">
            <span>MESSAGE</span>
            <textarea {..._getFormCommonProps(form, setForm, 'message')}>
              {form.message}
            </textarea>
            {_renderError(errors, 'message')}
            <br />
          </div>
        </div>
        <Button title="SEND" variant="blue" onClick={() => {}} />
      </form>
    </div>
  );
};

export default ContactForm;
