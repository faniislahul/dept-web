import React from 'react';

import Logos from '../../Assets/Logos';
import './Clients.component.styles.scss';

const _renderLogos = () => (
  <div className="logosContainer">
    {Object.keys(Logos).map((key) => (
      <div className="logosBox" key={Math.random()}>
        <img className="logos" src={Logos[key]} alt={key} />
      </div>
    ))}
  </div>
);

const Clients = () => (
  <div className="clientsContainer">
    <h1 className="clientsHeader">CLIENTS</h1>
    <p className="clientsText">
      {
        'We value a great working relationship with our clients above all else. It’s why '
        + 'they often come to our parties. It’s also why we’re able to challenge and '
        + 'inspire them to reach for the stars.'
      }
    </p>
    {_renderLogos()}
  </div>
);

export default Clients;
