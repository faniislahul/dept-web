import React from 'react';
import { render } from '@testing-library/react';

import Clients from './Clients.component';

jest.useFakeTimers();

describe('Clients Component test', () => {
  it('should return correct snapshot', () => {
    const { container } = render(<Clients />);

    expect(container.firstChild).toMatchSnapshot();
  });
});
