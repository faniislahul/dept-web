import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

import Filter from '../Filter';
import Button from '../Button';

import { actions } from '../../Redux/Reducer/Projects.reducer';
import './ProjectList.component.styles.scss';

const { setPage } = actions;

const _onLoadMore = (page, dispatch) => () => {
  dispatch(setPage(page + 1));
};

const _renderLoadMore = (page, dispatch) => (
  <div className="loadMoreContainer">
    <Button onClick={_onLoadMore(page, dispatch)} title="LOAD MORE" variant="black" />
  </div>
);

const _renderLoading = (loading) => (
  <div className="loadingContainer" data-show={loading}>
    <div className="loading">
      <div className="loader" />
      <span>FETCHING DATA</span>
    </div>
  </div>
);

const _renderProjectList = (projects = []) => (
  <div className="projectListContainer">
    {projects.map(({
      _id, clientName, title, imageUrl
    }) => (
      <div className="projectListItem" key={_id}>
        <div className="projectListImage" style={{ backgroundImage: `url(${imageUrl})` }} />
        <span>{clientName}</span>
        <h4>{title}</h4>
        <a href={`/projects/${_id}`}>
          <div className="triangle" />
          VIEW CASE
        </a>
      </div>
    ))}
  </div>
);

const ProjectList = () => {
  const {
    projects, loading, remaining, page
  } = useSelector((state) => state.project);
  const dispatch = useDispatch();

  return (
    <>
      <Filter />
      {_renderLoading(loading)}
      {_renderProjectList(projects)}
      {remaining > 0 && _renderLoadMore(page, dispatch)}
    </>
  );
};

export default ProjectList;
