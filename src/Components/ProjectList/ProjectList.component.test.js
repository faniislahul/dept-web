import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { useSelector, useDispatch } from 'react-redux';

import ProjectList from './ProjectList.component';

jest
  .mock('react-redux', () => ({
    useSelector: jest.fn(),
    useDispatch: jest.fn()
  }));

jest.useFakeTimers();

describe('ProjectList Component test', () => {
  const mockSelectorState = {
    projects: [],
    loading: false,
    remaining: 0,
    page: 1
  };

  describe('Snapshots test', () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });

    it('should return correct snapshot with default value', () => {
      useSelector.mockReturnValue(mockSelectorState);

      const { baseElement } = render(<ProjectList />);

      expect(baseElement).toMatchSnapshot();
    });

    it('should return correct snapshot with project list', () => {
      const newState = {
        ...mockSelectorState,
        projects: [
          {
            _id: '61aa1443540bbdff4e6976b8',
            title: 'Rethinking the entire online ecosystem',
            imageUrl: 'https://',
            category: 'apps',
            industry: 'internet',
            clientName: 'Florensis'
          }
        ]
      };
      useSelector.mockReturnValue(newState);

      const { baseElement } = render(<ProjectList />);

      expect(baseElement).toMatchSnapshot();
    });

    it('should return correct snapshot when projects is undefined', () => {
      const newState = {
        ...mockSelectorState,
        projects: undefined
      };
      useSelector.mockReturnValue(newState);

      const { baseElement } = render(<ProjectList />);

      expect(baseElement).toMatchSnapshot();
    });

    it('should return correct snapshot when loading is true', () => {
      const newState = {
        ...mockSelectorState,
        loading: true
      };
      useSelector.mockReturnValue(newState);

      const { baseElement } = render(<ProjectList />);

      expect(baseElement).toMatchSnapshot();
    });

    it('should return correct snapshot when remaining is more then 0', () => {
      const newState = {
        ...mockSelectorState,
        remaining: 3
      };
      useSelector.mockReturnValue(newState);

      const { baseElement } = render(<ProjectList />);

      expect(baseElement).toMatchSnapshot();
    });
  });

  it('should dispatch action when load more button clicked', () => {
    const newState = {
      ...mockSelectorState,
      remaining: 3
    };
    const mockDispatch = jest.fn();
    useSelector.mockReturnValue(newState);
    useDispatch.mockReturnValue(mockDispatch);

    const { getByTestId } = render(<ProjectList />);
    const component = getByTestId('button');

    fireEvent.click(component);

    expect(mockDispatch).toBeCalled();
  });
});
