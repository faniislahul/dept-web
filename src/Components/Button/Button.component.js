import React from 'react';
import PropTypes from 'prop-types';

import './Button.component.styles.scss';

const Button = ({ variant, onClick, title }) => (
  <div
    data-testid="button"
    className="button"
    data-variant={variant}
    onClick={onClick}
    onKeyPress={onClick}
    role="button"
    tabIndex="0"
    focusable
  >
    <span>{title}</span>
  </div>
);

Button.propTypes = {
  variant: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired
};

export default Button;
