import { createSlice } from '@reduxjs/toolkit';

// Reducers

/**
 * commonHandler -> common redux handler
 * @param {string} key - state object
 * @returns {Function} reducer handler
 */
export const commonHandler = (key) => (state, { payload }) => {
  state[key] = payload;
};

export const ClientSlice = createSlice({
  name: 'project',
  initialState: {
    projects: [],
    page: 1,
    remaining: 0,
    loading: false,
    error: false,
    industry: null,
    category: null
  },
  reducers: {
    setProjects: commonHandler('projects'),
    setError: commonHandler('error'),
    setLoading: commonHandler('loading'),
    setPage: commonHandler('page'),
    setRemaining: commonHandler('remaining'),
    setIndustry: commonHandler('industry'),
    setCategory: commonHandler('category'),
  },
});

export const { actions } = ClientSlice;

export default ClientSlice.reducer;
