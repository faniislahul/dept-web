import { commonHandler } from './Projects.reducer';

describe('Common handler', () => {
  it('should change the state', () => {
    const initialState = {
      projects: [],
      page: 1,
      remaining: 0,
      loading: false,
      error: false,
      industry: null,
      category: null
    };
    const expectedResult = {
      ...initialState,
      loading: true
    };

    const mockKey = 'loading';

    commonHandler(mockKey)(initialState, { payload: true });

    expect(initialState).toStrictEqual(expectedResult);
  });
});
