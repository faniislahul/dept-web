import { configureStore } from '@reduxjs/toolkit';

import projectReducer from './Reducer/Projects.reducer';

export default configureStore({
  reducer: {
    project: projectReducer,
  },
});
