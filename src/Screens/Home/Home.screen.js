import React from 'react';
import { useLocation } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

import { actions } from '../../Redux/Reducer/Projects.reducer';
import { get } from '../../Services/API.services';
import Header from '../../Components/Header';
import Footer from '../../Components/Footer';
import Clients from '../../Components/Clients';
import ContactForm from '../../Components/ContactForm';
import ProjectList from '../../Components/ProjectList';
import Button from '../../Components/Button';

// Assets
import './Home.screen.styles.scss';
import headerImage from '../../Assets/Images/Header.jpg';

const {
  setLoading,
  setProjects,
  setError,
  setIndustry,
  setCategory,
  setPage,
  setRemaining
} = actions;

const noop = () => {};

const _constructParams = (industry, category, page) => {
  let params = '';

  if (industry) params += `industry=${industry}&`;

  if (category) params += `category=${category}&`;

  if (page) params += `page=${page}`;

  return industry || category || page ? `?${params}` : params;
};

const _getQueryParams = (query, dispatch) => {
  const industry = query.get('industry');
  const category = query.get('category');

  if (industry) dispatch(setIndustry(industry));
  if (category) dispatch(setCategory(category));

  return _constructParams(industry, category, null);
};

const _onNetworkStart = (dispatch) => {
  dispatch(setError(false));
  dispatch(setLoading(true));
};

const _onNetworkError = (dispatch) => () => {
  dispatch(setError(true));
  dispatch(setLoading(false));
};

const _onNetworkSuccess = (dispatch) => (result) => {
  dispatch(setProjects(result.data.data));
  dispatch(setRemaining(result.data.remaining));
  dispatch(setLoading(false));
};

const _onLoadMoreSuccess = (dispatch, projects) => (result) => {
  dispatch(setProjects([...projects, ...result.data.data]));
  dispatch(setRemaining(result.data.remaining));
  dispatch(setLoading(false));
};

const _onStart = (query, dispatch, { loading }, setInit) => () => {
  if (loading) return;

  const params = _getQueryParams(query, dispatch);

  setInit(true);
  _onNetworkStart(dispatch);
  get(params, _onNetworkSuccess(dispatch), _onNetworkError(dispatch));
};

const _onCategoryChanges = (dispatch, { industry, category }, init) => () => {
  if (!init) return;

  const params = _constructParams(industry, category, 1);

  _onNetworkStart(dispatch);
  dispatch(setPage(1));
  get(params, _onNetworkSuccess(dispatch), _onNetworkError(dispatch));
};

const _onPageChanges = (dispatch, states, init) => () => {
  const {
    industry,
    category,
    page,
    projects,
    loading
  } = states;

  if (!init || loading) return;

  const params = _constructParams(industry, category, page);

  _onNetworkStart(dispatch);
  get(params, _onLoadMoreSuccess(dispatch, projects), _onNetworkError(dispatch));
};

const _renderBanner = () => (
  <>
    <div
      className="bannerContainer"
      style={{
        background: `url(${headerImage})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundPosition: 'center'
      }}
    >
      <h1 className="bannerText" data-text="WORK">WORK</h1>
      <div className="bannerAction"><Button title="VIEW CASE" variant="black" onClick={noop} /></div>
    </div>
    <div className="mobileAction"><Button title="VIEW CASE" variant="black" onClick={noop} /></div>
  </>
);

const useQuery = () => {
  const { search } = useLocation();

  return React.useMemo(() => new URLSearchParams(search), [search]);
};

const useNetworkEffect = (dispatch, query, states) => {
  const { industry, category, page } = states;
  const [init, setInit] = React.useState(false);

  React.useEffect(_onStart(query, dispatch, states, setInit), []);

  React.useEffect(_onCategoryChanges(dispatch, states, init), [industry, category]);

  React.useEffect(_onPageChanges(dispatch, states, init), [page]);
};

const HomeScreen = () => {
  const states = useSelector((state) => state.project);
  const dispatch = useDispatch();
  const query = useQuery();

  useNetworkEffect(dispatch, query, states);

  return (
    <div className="homeContainer">
      <Header />
      {_renderBanner()}
      <ProjectList />
      <Clients />
      <ContactForm />
      <Footer />
    </div>
  );
};

export default HomeScreen;
