import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { useSelector, useDispatch } from 'react-redux';
import axios from 'axios';
import { actions } from '../../Redux/Reducer/Projects.reducer';

import Home from './Home.screen';

jest
  .mock('react-redux', () => ({
    useSelector: jest.fn(),
    useDispatch: jest.fn()
  }))
  .mock('react-router-dom', () => ({
    useLocation: jest.fn().mockReturnValue({
      search: jest.fn()
    })
  }))
  .mock('../../Components/Footer', () => 'div')
  .mock('axios', () => ({
    get: jest.fn().mockReturnValue(Promise.resolve())
  }))
  .mock('../../Redux/Reducer/Projects.reducer', () => ({
    actions: {
      setLoading: jest.fn().mockImplementation((payload) => payload),
      setProjects: jest.fn().mockImplementation((payload) => payload),
      setError: jest.fn().mockImplementation((payload) => payload),
      setIndustry: jest.fn().mockImplementation((payload) => payload),
      setCategory: jest.fn().mockImplementation((payload) => payload),
      setPage: jest.fn().mockImplementation((payload) => payload),
      setRemaining: jest.fn().mockImplementation((payload) => payload)
    }
  }));

const {
  setError,
  setLoading,
  setIndustry,
  setCategory
} = actions;

jest.useFakeTimers();

describe('Home Screen test', () => {
  const mockSelectorState = {
    projects: [
      {
        _id: '61aa1443540bbdff4e6976b8',
        title: 'Rethinking the entire online ecosystem',
        imageUrl: 'https://',
        category: 'apps',
        industry: 'internet',
        clientName: 'Florensis'
      }
    ],
    loading: false,
    remaining: 0,
    page: 1
  };

  it('should return correct snapshot', () => {
    useSelector.mockReturnValue(mockSelectorState);
    useDispatch.mockReturnValue(jest.fn());

    const { container } = render(<Home />);

    expect(container.firstChild).toMatchSnapshot();
  });

  describe('Network calls test', () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });
    const mockResult = {
      data: {
        data: [
          {
            _id: '61aa1443540bbdff4e6976b8',
            title: 'Rethinking the entire online ecosystem',
            imageUrl: 'https://',
            category: 'apps',
            industry: 'internet',
            clientName: 'Florensis'
          }
        ],
        remaining: 0
      }
    };

    it('should successfully fetch data when component start', () => {
      const mockDispatch = jest.fn();
      useSelector.mockReturnValue(mockSelectorState);
      useDispatch.mockReturnValue(mockDispatch);
      axios.get.mockReturnValue(Promise.resolve(mockResult));

      render(<Home />);

      expect(mockDispatch).toBeCalledWith(setError(false));
      expect(mockDispatch).toBeCalledWith(setLoading(true));
    });

    it('should not fetch data when loading is true', () => {
      const mockDispatch = jest.fn();
      const mockState = {
        ...mockSelectorState,
        loading: true
      };
      useSelector.mockReturnValue(mockState);
      useDispatch.mockReturnValue(mockDispatch);
      axios.get.mockReturnValue(Promise.resolve(mockResult));

      render(<Home />);

      expect(mockDispatch).not.toBeCalledWith(setError(false));
      expect(mockDispatch).not.toBeCalledWith(setLoading(true));
    });

    it('should fetch data when filter selected', () => {
      const mockDispatch = jest.fn();
      useSelector.mockReturnValue(mockSelectorState);
      useDispatch.mockReturnValue(mockDispatch);
      axios.get.mockReturnValue(Promise.resolve(mockResult));
      jest.spyOn(React, 'useState').mockReturnValue([true, jest.fn()]);

      const { getByTestId } = render(<Home />);
      const div = getByTestId('options-websites');
      fireEvent.click(div);

      expect(mockDispatch).toBeCalledWith(setError(false));
      expect(mockDispatch).toBeCalledWith(setLoading(true));
    });

    it('should set category and industry from url params', () => {
      const mockDispatch = jest.fn();
      const mockIndustry = 'banking';
      const mockCategory = 'websites';
      useSelector.mockReturnValue(mockSelectorState);
      useDispatch.mockReturnValue(mockDispatch);
      axios.get.mockReturnValue(Promise.resolve(mockResult));
      jest.spyOn(React, 'useState').mockReturnValue([true, jest.fn()]);
      jest.spyOn(URLSearchParams.prototype, 'get').mockReturnValueOnce(mockCategory);
      jest.spyOn(URLSearchParams.prototype, 'get').mockReturnValueOnce(mockIndustry);

      render(<Home />);

      expect(mockDispatch).toBeCalledWith(setCategory(mockCategory));
      expect(mockDispatch).toBeCalledWith(setIndustry(mockIndustry));
    });
  });
});
