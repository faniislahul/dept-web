import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Home from '../Screens/Home';

const Routers = () => (
  <Switch>
    <Route path="/" exact>
      <Home />
    </Route>
  </Switch>
);

export default Routers;
